package org.github.foxnic.web.framework;

public class FoxnicWebMeta {

	/**
	 *
	 * */
	public static final String FEIGN_AUTO_CONFIGURATION="org.springframework.cloud.openfeign.FeignAutoConfiguration";
	/**
	 * 基础包
	 * */
	public static final String BASE_PACKAGE="org.github.foxnic.web";
 
	/**
	 * Feign代理包
	 * */
	public static final String PROXY_PACKAGE=BASE_PACKAGE+".proxy";
	
	/**
	 * 公共框架包
	 * */
	public static final String FRAMEWORK_PACKAGE=BASE_PACKAGE+".framework";
	
	/**
	 * 实体包
	 * */
	public static final String DOMAIN_PACKAGE=BASE_PACKAGE+".doman";
	
	
	/**
	 * 系统服务
	 * */
	public static final String SERVICE_SYSTEM_PACKAGE=BASE_PACKAGE+".system";
	
	
	/**
	 * 认证服务
	 * */
	public static final String SERVICE_OAUTH_PACKAGE=BASE_PACKAGE+".oauth";


	/**
	 * 存储服务
	 * */
	public static final String SERVICE_STORAGE_PACKAGE=BASE_PACKAGE+".storage";

	/**
	 * 人事基础服务
	 * */
	public static final String SERVICE_HRM_PACKAGE=BASE_PACKAGE+".hrm";

	/**
	 * 属性自定义服务
	 * */
	public static final String SERVICE_PCM_PACKAGE=BASE_PACKAGE+".pcm";


	/**
	 * Support
	 * */
	public static final String WRAPPER_SUPPORT_PACKAGE=BASE_PACKAGE+".wrapper.support";
	
	
	 
 
	
}

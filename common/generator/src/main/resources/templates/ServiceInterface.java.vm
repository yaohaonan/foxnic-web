package #(package);

#(imports)

#(classJavaDoc)

public interface #(simpleName) extends ISuperService<#(poSimpleName)> {
	
	/**
	 * 插入实体
	 * @param #(poVar) 实体数据
	 * @return 插入是否成功
	 * */
	Result insert(#(poSimpleName) #(poVar));
 
	/**
	 * 批量插入实体，事务内
	 * @param #(poListVar) 实体数据清单
	 * @return 插入是否成功
	 * */
	Result insertList(List<#(poSimpleName)> #(poListVar));
	
	
	#(deleteByIdMethods)
	
	#if(isSimplePK)
	/**
	 * 批量物理删除，仅支持单字段主键表
	 * @param ids 主键清单
	 * @return 是否删除成功
	 * */
	<T> Result deleteByIdsPhysical(List<T> #(idPropertyName)s);
	
	/**
	 * 批量逻辑删除，仅支持单字段主键表
	 * @param ids 主键清单
	 * @return 是否删除成功
	 * */
	<T> Result deleteByIdsLogical(List<T> #(idPropertyName)s);
	#end
	
	#(updateByIdMethod)
	
	/**
	 * 更新实体
	 * @param #(poVar) 数据对象
	 * @param mode 保存模式
	 * @return 保存是否成功
	 * */
	Result update(#(poSimpleName) #(poVar) , SaveMode mode);
	
	
	/**
	 * 更新实体集，事务内
	 * @param #(poListVar) 数据对象列表
	 * @param mode 保存模式
	 * @return 保存是否成功
	 * */
	Result updateList(List<#(poSimpleName)> #(poListVar), SaveMode mode);
	
	/**
	 * 保存实体，如果主键值不为 null，则更新，否则插入
	 * @param #(poVar) 实体数据
	 * @param mode 保存模式
	 * @return 保存是否成功
	 * */
	Result save(#(poSimpleName) #(poVar) , SaveMode mode);
	
	/**
	 * 保存实体，如果主键值不为null，则更新，否则插入
	 * @param #(poListVar) 实体数据清单
	 * @param mode 保存模式
	 * @return 保存是否成功
	 * */
	Result saveList(List<#(poSimpleName)> #(poListVar) , SaveMode mode);
	
	/**
	 * 检查实体中的数据字段是否已经存在
	 * @param #(poVar)  实体对象
	 * @param field  字段清单，至少指定一个
	 * @return 是否已经存在
	 * */
	boolean checkExists(#(poSimpleName) #(poVar),DBField... field);
 
	#(getByIdMethod)
	#if(isSimplePK)
		
	/**
	 * 检查实体中的数据字段是否已经存在
	 * @param #(idPropertyName)s  主键清单
	 * @return 实体集
	 * */
	List<#(poSimpleName)> getByIds(List<#(idPropertyType)> #(idPropertyName)s);
	#end

	/**
	 * 检查 角色 是否已经存在
	 *
	 * @param #(poVar) 数据对象
	 * @return 判断结果
	 */
	Result<#(poSimpleName)> checkExists(#(poSimpleName) #(poVar));

	/**
	 * 根据实体数构建默认的条件表达式，字符串使用模糊匹配
	 * @param sample 数据样例
	 * @return ConditionExpr 条件表达式
	 * */
	ConditionExpr buildQueryCondition(#(poSimpleName) sample);
	
	/**
	 * 根据实体数构建默认的条件表达式, 字符串是否使用模糊匹配
	 * @param sample 数据样例
	 * @param tableAliase 数据表别名
	 * 	@return ConditionExpr 条件表达式
	 * */
	ConditionExpr buildQueryCondition(#(poSimpleName) sample,String tableAliase);

	/**
	 * 查询实体集合，默认情况下，字符串使用模糊匹配，非字符串使用精确匹配
	 * @param sample  查询条件
	 * @return 查询结果
	 * */
	List<#(poSimpleName)> queryList(#(poSimpleName) sample);
 
	/**
	 * 查询实体集合，默认情况下，字符串使用模糊匹配，非字符串使用精确匹配
	 * @param sample  查询条件
	 * @param condition  其它条件
	 * @param orderBy  排序
	 * @return 查询结果
	 * */
	List<#(poSimpleName)> queryList(#(poSimpleName) sample,ConditionExpr condition,OrderBy orderBy);
	
	/**
	 * 查询实体集合，默认情况下，字符串使用模糊匹配，非字符串使用精确匹配
	 * @param sample  查询条件
	 * @param orderBy  排序
	 * @return 查询结果
	 * */
	List<#(poSimpleName)> queryList(#(poSimpleName) sample,OrderBy orderBy);
	
	/**
	 * 查询实体集合，默认情况下，字符串使用模糊匹配，非字符串使用精确匹配
	 * @param sample  查询条件
	 * @param condition  其它条件
	 * @return 查询结果
	 * */
	List<#(poSimpleName)> queryList(#(poSimpleName) sample,ConditionExpr condition);
	
	/**
	 * 查询单个实体
	 * @param sample  查询条件
	 * @return 查询结果
	 * */
	#(poSimpleName) queryEntity(#(poSimpleName) sample);
	
	/**
	 * 分页查询实体集
	 * @param sample  查询条件
	 * @param pageSize 分页条数
	 * @param pageIndex 页码
	 * @return 查询结果
	 * */
	PagedList<#(poSimpleName)> queryPagedList(#(poSimpleName) sample,int pageSize,int pageIndex);
	
	/**
	 * 分页查询实体集
	 * @param sample  查询条件
	 * @param pageSize 分页条数
	 * @param pageIndex 页码
	 * @param condition  其它条件
	 * @param orderBy  排序
	 * @return 查询结果
	 * */
	PagedList<#(poSimpleName)> queryPagedList(#(poSimpleName) sample,ConditionExpr condition,OrderBy orderBy,int pageSize,int pageIndex);
	
	/**
	 * 分页查询实体集
	 * @param sample  查询条件
	 * @param pageSize 分页条数
	 * @param pageIndex 页码
	 * @param condition  其它条件
	 * @return 查询结果
	 * */
	PagedList<#(poSimpleName)> queryPagedList(#(poSimpleName) sample,ConditionExpr condition,int pageSize,int pageIndex);
	
	/**
	 * 分页查询实体集
	 * @param sample  查询条件
	 * @param pageSize 分页条数
	 * @param pageIndex 页码
	 * @param orderBy  排序
	 * @return 查询结果
	 * */
	PagedList<#(poSimpleName)> queryPagedList(#(poSimpleName) sample,OrderBy orderBy,int pageSize,int pageIndex);
 
 	/**
	 * 查询指定字段的数据清单
	 * @param <T> 元素类型
	 * @param field 字段
	 * @param type 元素类型
	 * @param condition 条件表达式
	 * @return 列数据
	 * */
	<T> List<T> queryValues(DBField field,Class<T> type, ConditionExpr condition);
 
	/**
	 * 查询指定字段的数据清单
	 * @param <T> 元素类型
	 * @param field 字段
	 * @param type 元素类型
	 * @param condition 条件表达式
	 * @param ps 参数清单
	 * @return 列数据
	 * */
	<T> List<T> queryValues(DBField field, Class<T> type, String condition,Object... ps);

	/**
	 * 导出 Excel
	 * */
	ExcelWriter exportExcel(#(poSimpleName) sample);

	/**
	 * 导出用于数据导入的 Excel 模版
	 * */
	ExcelWriter  exportExcelTemplate();

	/**
	 * 构建 Excel 结构
	 * @param  isForExport 是否用于数据导出
	 * @return   ExcelStructure
	 * */
	ExcelStructure buildExcelStructure(boolean isForExport);

	/**
	 * 导入 Excel 数据
	 * @return  错误信息，成功时返回 null
	 * */
	List<ValidateResult> importExcel(InputStream input,int sheetIndex,boolean batch);

	#if(relationMasterIdField!=null)
	/**
	 * 保存关系
	 * @param #(relationMasterVar) #(relationMasterVarDoc)
	 * @param #(relationSlaveVar) #(relationSlaveVarDoc)
	 */
	void saveRelation(#(relationMasterVarType) #(relationMasterVar),List<#(relationSlaveVarType)> #(relationSlaveVar));
	#end
 
}
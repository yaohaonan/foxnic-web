package org.github.foxnic.web.proxy;

public class MicroServiceNames {
	
	/**
	 * 系统相关的服务 <br> 
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String SYSTEM="service-system";
	
	
	/**
	 * 认证中心服务 <br> 
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String OAUTH="service-oauth";
	
	
	
	/**
	 * 存储相关的服务 <br> 
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String STORAGE="service-storage";
	
	/**
	 * 组织人事相关的服务 <br> 
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String HRM="service-hrm";

	/**
	 * 自定义属性相关的服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String PCM="service-pcm";
 
}

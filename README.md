# Foxnic-Web

#### 介绍
应用开发基础框架，基于数据库表结构生成模块代码。通过Wrapper项目聚合，即可作为单体应用部署，也可作为微服务节点部署。

#### 软件架构
后续完善

#### 主要技术栈
Spring Boot 

Spring Cloud

Spring Security

[foxnic](https://gitee.com/LeeFJ/foxnic)  ， [文档（老版本）](https://gitee.com/LeeFJ/tity-sql-public/wikis/)



Theamleaf

Jquery

LayUI

#### 安装教程

后续完善

#### 使用说明

后续完善

#### 参与贡献

后续完善

